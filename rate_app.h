#ifndef RATE_APP_H_
#define RATE_APP_H_

#include "collector.h"

#include "app_context.h"
#include "counter.h"
#include <vector>

class RateApp final : public CollectorBase {
 public:
  RateApp(AppContext& app_ctx)
      : CollectorBase(app_ctx),
        app_ctx_{app_ctx},
        counter_{app_ctx.counter_names(), "tx_prio3_bytes"} {}

  virtual ~RateApp() {};

  virtual void Consume(CQE* cqe) {
    counter_.Update(cqe);
  }

 private:
  AppContext& app_ctx_;
  Counter counter_;
};

#endif  // RATE_APP_H_