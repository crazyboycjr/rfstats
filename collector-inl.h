#ifndef COLLECTOR_INL_H_
#define COLLECTOR_INL_H_

#include "collector.h"

class CollectorBase : public ICollector {
 public:
  CollectorBase(AppContext& app_ctx) : app_ctx_{app_ctx} {}

  virtual ~CollectorBase() {}

  virtual void PollOnce() {
    // poll from all threads
    int num_workers = app_ctx_.worker_pool()->num_workers();
    for (int i = 0; i < num_workers; i++) {
      CQE cqe;
      auto& cq = app_ctx_.worker_pool()->worker_thread(i)->cq();
      while (cq.TryPop(&cqe)) {
        Consume(&cqe);
      }
    }
  }

 private:
  AppContext& app_ctx_;
};

typedef std::vector<CQE> CQEBatch;

class CollectorBatch : public ICollector {
 public:
  CollectorBatch(AppContext& app_ctx, size_t batch_size)
      : app_ctx_{app_ctx}, batch_size_{batch_size} {}

  virtual ~CollectorBatch() {}

  virtual void ConsumeBatch(const CQEBatch& batch, size_t num) = 0;

  virtual void PollOnce() {
    int num_workers = app_ctx_.worker_pool()->num_workers();
    buffer.resize(batch_size_);
    size_t buffer_num = 0;
    for (int i = 0; i < num_workers; i++) {
      CQE cqe;
      auto& cq = app_ctx_.worker_pool()->worker_thread(i)->cq();
      while (cq.TryPop(&cqe)) {
        buffer[buffer_num++] = std::move(cqe);
        if (buffer_num >= batch_size_) {
          ConsumeBatch(buffer, buffer_num);
          buffer_num -= batch_size_;
        }
      }
    }
    if (buffer_num > 0) ConsumeBatch(buffer, buffer_num);
  }

 private:
  AppContext& app_ctx_;
  size_t batch_size_;
  CQEBatch buffer;
};

#endif  // COLLECTOR_INL_H_