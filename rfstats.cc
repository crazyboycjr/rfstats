#include <string>
#include <vector>
#include <chrono>
#include <thread>

#include <memory>
#include <functional>

#include <signal.h>

#include "ethtool.h"
#include "worker_thread.h"
#include "config.h"
#include "app_context.h"
#include "collector.h"
#include "worker_pool.h"
#include "rate_app.h"

void Usage(const char *app) {
  fprintf(stdout, "Usage:\n");
  fprintf(stdout, "  %s <intf>    read statistics of <intf>\n", app);
  fprintf(stdout, "\nOptions:\n");
  fprintf(stdout, "  -i, --interval=<int>    watch interval in us, default is 1000\n");
  fprintf(stdout, "  -C, --concurrency=<int> number of worker threads, default is 4\n");
  fprintf(stdout, "  -D, --duration=<int>    run for a customized period of seconds\n");
}

inline void ExitBadArgs(const char* app)
{
	fprintf(stderr, "bad command line argument(s)\n");
  Usage(app);
	exit(1);
}

AppContext::AppContext() : conf_{}, cmd_ctx_{}, counter_names_{nullptr} {}

AppContext::~AppContext() {
  if (counter_names_) {
    free(counter_names_);
    counter_names_ = nullptr;
  }
}

int AppContext::Run() {
  struct cmd_context& ctx = cmd_ctx();

  /* Setup our control structures. */
  memset(&ctx.ifr, 0, sizeof(ctx.ifr));
  strcpy(ctx.ifr.ifr_name, conf_.devname_);

  /* Open control socket. */
  ctx.fd = socket(AF_INET, SOCK_DGRAM, 0);
  if (ctx.fd < 0)
    ctx.fd = socket(AF_NETLINK, SOCK_RAW, NETLINK_GENERIC);
  if (ctx.fd < 0) {
    perror("Cannot get control socket");
    return 70;
  }

  /* Get counter names */
  counter_names_ = get_stringset(&cmd_ctx_, ETH_SS_STATS,
                                 offsetof(struct ethtool_drvinfo, n_stats), 0);
  if (!counter_names_) {
    perror("Cannot get stats counter names information");
    return 96;
  }

  /*
                  >-- worker thread 1 -->
    main thread -->-- worker thread 2 -->-- collector thread
                  >-- worker thread n -->
  */

  /* Start worker threads */
  int concurrency = conf_.concurrency_;
  LOG(INFO) << "concurrency is: " << concurrency;

  worker_pool_ = std::make_unique<WorkerPool>(*this, concurrency);
  worker_pool_->CreateWorkerThreads();

  worker_pool_->StartWorkerThreads();
  /* Set worker threads affinities */

  /* Start collector thread */
  ICollector* rate_app = new RateApp(*this);
  collector_th_ = std::make_unique<CollectorThread>(rate_app);
  collector_th_->Start();

  /* Main thread push requests */
  auto start = Clock::now();
  int num_workers = worker_pool()->num_workers();

  // int numw[num_workers];
  // memset(numw, 0, sizeof(numw));
  while (1) {
    for (int i = 0; i < num_workers; i++) {
      WQE wqe{};
      auto& wq = worker_pool()->worker_thread(i)->wq();
      while (!wq.TryPush(wqe)) {
      }
      // printf("send wqe to worker %d, %d\n", i, ++numw[i]);
    }

    if (conf_.duration() > 0 &&
        (Clock::now() - start).count() > conf_.duration() * 1e9)
      break;
  }

  return Exit();
}

int AppContext::Exit() {
  /* Join threads */
  if (collector_th_) {
    collector_th_->Terminate();
    collector_th_->Join();
  }
  if (worker_pool_) {
    worker_pool_->StopWorkerThreads();
    for (int i = 0; i < worker_pool_->num_workers(); i++) {
      worker_pool_->worker_thread(i)->Join();
    }
  }
  return 0;
}

int exit_handler(int sig, void* app_ctx) {
  static AppContext* inst = nullptr;
  if (!inst) {
    inst = static_cast<AppContext*>(app_ctx);
    return 0;
  }
  LOG(INFO) << "signal " << sig << " received, exiting...";
  exit(inst->Exit());
}

int main(int argc, char* argv[]) {
  AppContext app_ctx;

  if (app_ctx.conf().ParseArgument(argc, argv)) {
    ExitBadArgs(argv[0]);
    return 1;
  }

  exit_handler(0, &app_ctx);
  signal(SIGINT, (void (*)(int))exit_handler);

  return app_ctx.Run();
}
