#ifndef COLLECTOR_H_
#define COLLECTOR_H_

#include "thread_proto.h"
#include "worker_thread.h"
#include "worker_pool.h"

/*!
 * \brief collector interface for different collector applications.
 */
class ICollector {
 public:
  ICollector() {}

  virtual ~ICollector() {}

  virtual void PollOnce() = 0;

  virtual void Consume(CQE* cqe) = 0;
};

class CollectorThread final : public TerminableThread {
 public:
  CollectorThread(ICollector* collector) : collector_{collector} {}

  virtual ~CollectorThread() {}

  virtual void Run() {
    while (!terminated_.load()) {
      collector_->PollOnce();
    }
  }
 private:
  std::unique_ptr<ICollector> collector_;
};

#include "collector-inl.h"

#endif  // COLLECTOR_H_