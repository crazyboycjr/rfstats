#ifndef WORKER_THREAD_H_
#define WORKER_THREAD_H_

#include "thread_proto.h"
#include "spsc_queue.h"
#include "ethtool.h"
#include "app_context.h"
#include "prism/logging.h"

struct WQE {
};

using Clock = std::chrono::high_resolution_clock;

struct CQE {
  __u64 rx_bytes[PRIO_NUM];
  __u64 rx_packs[PRIO_NUM];
  __u64 tx_bytes[PRIO_NUM];
  __u64 tx_packs[PRIO_NUM];
  long ts;
};

class WorkerThread final : public TerminableThread {
 public:
  WorkerThread(AppContext& app_ctx, const char* key);

  virtual ~WorkerThread();

  virtual void Run() override;

  inline SpscQueue<CQE>& cq() { return cq_; }
  inline SpscQueue<WQE>& wq() { return wq_; }

 private:
  inline int SendIfReq() {
    /*
    std::this_thread::sleep_for(std::chrono::milliseconds(2));
    return 0;
    */
    return ioctl(ctx_.fd, SIOCETHTOOL, &ctx_.ifr);
  }

  void LoadStats(CQE* cqe, size_t size);

  void PrepareRequest(unsigned int n_stats);

  void FindIndex(struct ethtool_gstrings* strings, const char* key);

  AppContext& app_ctx_;

  size_t index_;

  struct cmd_context ctx_;

  // int fd_;
  // struct ifreq ifr_;
  struct ethtool_stats* stats_;

  SpscQueue<WQE> wq_;
  SpscQueue<CQE> cq_;
};


#endif  // WORKER_THREAD_H_
