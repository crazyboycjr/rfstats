#ifndef MY_ETHTOOL_H_
#define MY_ETHTOOL_H_

#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <endian.h>
#include <sys/ioctl.h>
#include <net/if.h>

#include <sys/stat.h>
#include <stddef.h>
#include <errno.h>
#include <sys/utsname.h>
#include <limits.h>
#include <ctype.h>
#include <getopt.h>
#include <assert.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <linux/sockios.h>
#include <linux/netlink.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef unsigned long long u64;
typedef uint32_t u32;
typedef uint16_t u16;
typedef uint8_t u8;
typedef int32_t s32;

#define ETHTOOL_FWVERS_LEN	32
#define ETHTOOL_BUSINFO_LEN	32
#define ETHTOOL_EROMVERS_LEN	32

#define ETHTOOL_GSSET_INFO	0x00000037 /* Get string set info */
#define ETHTOOL_GSTATS		0x0000001d /* get NIC-specific statistics */
#define ETHTOOL_GDRVINFO	0x00000003 /* Get driver info. */
#define ETHTOOL_GSTRINGS	0x0000001b /* get specified string set */

#define ETH_GSTRING_LEN		32

/* Context for sub-commands */
struct cmd_context {
	int fd;			/* socket suitable for ethtool ioctl */
	struct ifreq ifr;	/* ifreq suitable for ethtool ioctl */
};

/**
 * enum ethtool_stringset - string set ID
 * @ETH_SS_TEST: Self-test result names, for use with %ETHTOOL_TEST
 * @ETH_SS_STATS: Statistic names, for use with %ETHTOOL_GSTATS
 * @ETH_SS_PRIV_FLAGS: Driver private flag names, for use with
 *	%ETHTOOL_GPFLAGS and %ETHTOOL_SPFLAGS
 * @ETH_SS_NTUPLE_FILTERS: Previously used with %ETHTOOL_GRXNTUPLE;
 *	now deprecated
 * @ETH_SS_FEATURES: Device feature names
 * @ETH_SS_RSS_HASH_FUNCS: RSS hush function names
 * @ETH_SS_PHY_STATS: Statistic names, for use with %ETHTOOL_GPHYSTATS
 * @ETH_SS_PHY_TUNABLES: PHY tunable names
 */
enum ethtool_stringset {
	ETH_SS_TEST		= 0,
	ETH_SS_STATS,
	ETH_SS_PRIV_FLAGS,
	ETH_SS_NTUPLE_FILTERS,
	ETH_SS_FEATURES,
	ETH_SS_RSS_HASH_FUNCS,
	ETH_SS_TUNABLES,
	ETH_SS_PHY_STATS,
	ETH_SS_PHY_TUNABLES,
};

/**
 * struct ethtool_gstrings - string set for data tagging
 * @cmd: Command number = %ETHTOOL_GSTRINGS
 * @string_set: String set ID; one of &enum ethtool_stringset
 * @len: On return, the number of strings in the string set
 * @data: Buffer for strings.  Each string is null-padded to a size of
 *	%ETH_GSTRING_LEN.
 *
 * Users must use %ETHTOOL_GSSET_INFO to find the number of strings in
 * the string set.  They must allocate a buffer of the appropriate
 * size immediately following this structure.
 */
struct ethtool_gstrings {
	__u32	cmd;
	__u32	string_set;
	__u32	len;
	__u8	data[0];
};

/**
 * struct ethtool_stats - device-specific statistics
 * @cmd: Command number = %ETHTOOL_GSTATS
 * @n_stats: On return, the number of statistics
 * @data: Array of statistics
 *
 * Users must use %ETHTOOL_GSSET_INFO or %ETHTOOL_GDRVINFO to find the
 * number of statistics that will be returned.  They must allocate a
 * buffer of the appropriate size (8 * number of statistics)
 * immediately following this structure.
 */
struct ethtool_stats {
	__u32	cmd;
	__u32	n_stats;
	__u64	data[0];
};

/**
 * struct ethtool_sset_info - string set information
 * @cmd: Command number = %ETHTOOL_GSSET_INFO
 * @sset_mask: On entry, a bitmask of string sets to query, with bits
 *	numbered according to &enum ethtool_stringset.  On return, a
 *	bitmask of those string sets queried that are supported.
 * @data: Buffer for string set sizes.  On return, this contains the
 *	size of each string set that was queried and supported, in
 *	order of ID.
 *
 * Example: The user passes in @sset_mask = 0x7 (sets 0, 1, 2) and on
 * return @sset_mask == 0x6 (sets 1, 2).  Then @data[0] contains the
 * size of set 1 and @data[1] contains the size of set 2.
 *
 * Users must allocate a buffer of the appropriate size (4 * number of
 * sets queried) immediately following this structure.
 */
struct ethtool_sset_info {
	__u32	cmd;
	__u32	reserved;
	__u64	sset_mask;
	__u32	data[0];
};

/**
 * struct ethtool_drvinfo - general driver and device information
 * @cmd: Command number = %ETHTOOL_GDRVINFO
 * @driver: Driver short name.  This should normally match the name
 *	in its bus driver structure (e.g. pci_driver::name).  Must
 *	not be an empty string.
 * @version: Driver version string; may be an empty string
 * @fw_version: Firmware version string; may be an empty string
 * @erom_version: Expansion ROM version string; may be an empty string
 * @bus_info: Device bus address.  This should match the dev_name()
 *	string for the underlying bus device, if there is one.  May be
 *	an empty string.
 * @n_priv_flags: Number of flags valid for %ETHTOOL_GPFLAGS and
 *	%ETHTOOL_SPFLAGS commands; also the number of strings in the
 *	%ETH_SS_PRIV_FLAGS set
 * @n_stats: Number of u64 statistics returned by the %ETHTOOL_GSTATS
 *	command; also the number of strings in the %ETH_SS_STATS set
 * @testinfo_len: Number of results returned by the %ETHTOOL_TEST
 *	command; also the number of strings in the %ETH_SS_TEST set
 * @eedump_len: Size of EEPROM accessible through the %ETHTOOL_GEEPROM
 *	and %ETHTOOL_SEEPROM commands, in bytes
 * @regdump_len: Size of register dump returned by the %ETHTOOL_GREGS
 *	command, in bytes
 *
 * Users can use the %ETHTOOL_GSSET_INFO command to get the number of
 * strings in any string set (from Linux 2.6.34).
 *
 * Drivers should set at most @driver, @version, @fw_version and
 * @bus_info in their get_drvinfo() implementation.  The ethtool
 * core fills in the other fields using other driver operations.
 */
struct ethtool_drvinfo {
	__u32	cmd;
	char	driver[32];
	char	version[32];
	char	fw_version[ETHTOOL_FWVERS_LEN];
	char	bus_info[ETHTOOL_BUSINFO_LEN];
	char	erom_version[ETHTOOL_EROMVERS_LEN];
	char	reserved2[12];
	__u32	n_priv_flags;
	__u32	n_stats;
	__u32	testinfo_len;
	__u32	eedump_len;
	__u32	regdump_len;
};

inline static
int send_ioctl(struct cmd_context *ctx, void *cmd)
{
	ctx->ifr.ifr_data = reinterpret_cast<char*>(cmd);
	return ioctl(ctx->fd, SIOCETHTOOL, &ctx->ifr);
}

inline static struct ethtool_gstrings*
get_stringset(struct cmd_context *ctx, enum ethtool_stringset set_id,
	      ptrdiff_t drvinfo_offset, int null_terminate)
{
	struct {
		struct ethtool_sset_info hdr;
		u32 buf[1];
	} sset_info;
	struct ethtool_drvinfo drvinfo;
	u32 len, i;
	struct ethtool_gstrings *strings;

	sset_info.hdr.cmd = ETHTOOL_GSSET_INFO;
	sset_info.hdr.reserved = 0;
	sset_info.hdr.sset_mask = 1ULL << set_id;
	if (send_ioctl(ctx, &sset_info) == 0) {
		len = sset_info.hdr.sset_mask ? sset_info.hdr.data[0] : 0;
	} else if (errno == EOPNOTSUPP && drvinfo_offset != 0) {
		/* Fallback for old kernel versions */
		drvinfo.cmd = ETHTOOL_GDRVINFO;
		if (send_ioctl(ctx, &drvinfo))
			return NULL;
		len = *(u32 *)((char *)&drvinfo + drvinfo_offset);
	} else {
		return NULL;
	}

	strings = static_cast<struct ethtool_gstrings*>(calloc(1, sizeof(*strings) + len * ETH_GSTRING_LEN));
	if (!strings)
		return NULL;

	strings->cmd = ETHTOOL_GSTRINGS;
	strings->string_set = set_id;
	strings->len = len;
	if (len != 0 && send_ioctl(ctx, strings)) {
		free(strings);
		return NULL;
	}

	if (null_terminate)
		for (i = 0; i < len; i++)
			strings->data[(i + 1) * ETH_GSTRING_LEN - 1] = 0;

	return strings;
}

inline static
void dump_stats(int n_stats, struct ethtool_gstrings* strings,
                struct ethtool_stats* stats, const char* name) {
  /* todo - pretty-print the strings per-driver */
  fprintf(stdout, "%s statistics:\n", name);
  for (int i = 0; i < n_stats; i++) {
    fprintf(stdout, "     %.*s: %llu\n",
            ETH_GSTRING_LEN,
            &strings->data[i * ETH_GSTRING_LEN],
            stats->data[i]);
  }
}

#ifdef __cplusplus
}  // extern "C"
#endif

#endif  // MY_ETHTOOL_H_