#ifndef CONFIG_H_
#define CONFIG_H_

#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>

#define PRIO_NUM 8
#define RX_PRIO0_BYTES "rx_prio0_bytes"
#define DEFAULT_INTERVAL_US 1000
#define DEFAULT_CONCURRENCY 4
#define DEFAULT_DURATION 0

// const size_t kPrioNum = 8;
// const char* kRxPrio0Bytes = "rx_prio0_bytes";
// const long kDefaultIntervalUs = 1000;

extern void Usage(const char *app);

class Config {
 public:
  friend class AppContext;
  int ParseArgument(int argc, char* argv[]);

  inline const char* devname() const { return devname_; }
  inline int concurrency() const { return concurrency_; }
  inline int duration() const { return duration_; }

 private:
	const char* devname_;	/* net device name */
  long interval_us_;
  int concurrency_;
  int duration_;
};

#endif  // CONFIG_H_
