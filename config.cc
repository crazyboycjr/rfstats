#include "config.h"

int Config::ParseArgument(int argc, char *argv[]) {
  int err = 0;
  static struct option long_options[] = {
      {"help", no_argument, 0, 'h'},               // NOLINT(*)
      {"interval", required_argument, 0, 'i'},     // NOLINT(*)
      {"concurrency", required_argument, 0, 'C'},  // NOLINT(*)
      {"duration", required_argument, 0, 'D'},  // NOLINT(*)
      {0, 0, 0, 0}                                 // NOLINT(*)
  };
  interval_us_ = DEFAULT_INTERVAL_US;
  concurrency_ = DEFAULT_CONCURRENCY;
  duration_ = DEFAULT_DURATION;

  while (1) {
    int option_index = 0, c;
    c = getopt_long(argc, argv, "hi:C:D:", long_options, &option_index);
    if (c == -1)
      break;
    switch (c) {
      case 'h': {
        Usage(argv[0]);
        exit(0);
      }
      case 'i': {
        interval_us_ = atol(optarg);
        break;
      }
      case 'C': {
        concurrency_ = atol(optarg);
        break;
      }
      case 'D': {
        duration_ = atol(optarg);
        break;
      }
      case '?':
      default:
        err = 1;
        goto out;
    }
  }
  if (optind < argc) {
    if (optind + 1 != argc) {
      err = 1;
      goto out;
    }
    devname_ = argv[optind];
  }
out:
  return err;
}
