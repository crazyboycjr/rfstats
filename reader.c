#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include <linux/ethtool.h>
#include <linux/sockios.h>

#include <sys/ioctl.h>

#include <net/if.h>

#define PRIO_NUM (8)

const char* devname = "rdma0";

void dump_stats(int n_stats, struct ethtool_gstrings* strings, struct ethtool_stats* stats) {
	fprintf(stdout, "NIC statistics:\n");
	for (int i = 0; i < n_stats; ++i) {
		fprintf(stdout, "     %.*s: %llu\n",
			ETH_GSTRING_LEN,
			&strings->data[i * ETH_GSTRING_LEN],
			stats->data[i]);
	}
}

/*
 * fixme: Directly read the prio* statistics by pre-known index 
 * 			- started from 109, with the order rx_prio*_bytes, rx_prio*_packets, tx_prio*_bytes, tx_prio*_packets
 * 		  Should get the index by matching the strings
 */
void load_stats(struct ethtool_gstrings* strings, 
				struct ethtool_stats* stats,
				__u64* tx_packs,
				__u64* tx_bytes,
				__u64* rx_packs,
				__u64* rx_bytes,
				size_t size) {
	size_t load_index = 109;
	for (size_t i = 0; i < size; ++i) {
		rx_bytes[i] = stats->data[load_index++];
		rx_packs[i] = stats->data[load_index++];
		tx_bytes[i] = stats->data[load_index++];
		tx_packs[i] = stats->data[load_index++];
	}
}

void prepare_request(unsigned int n_stats, struct ifreq* ifr) {
	unsigned int sz_stats = n_stats * sizeof(__u64);	

	struct ethtool_stats* stats;
	stats = calloc(1, sz_stats + sizeof(struct ethtool_stats));
	if(!stats) {
		fprintf(stderr, "no memory available\n");
		exit(1);
	}

	stats->cmd = ETHTOOL_GSTATS;
	stats->n_stats = n_stats;

	ifr->ifr_data = (caddr_t) stats;
}

void read_counters(int fd, struct ifreq* ifr) {
	if(ioctl(fd, SIOCETHTOOL, ifr) < 0) {
		perror("Cannot get stats info");
		exit(1);
	}
}

int main() {

	struct ifreq ifr;
	memset(&ifr, 0, sizeof(ifr));
	strcpy(ifr.ifr_name, devname);

	int fd = socket(AF_INET, SOCK_DGRAM, 0);
	if(fd < 0) {
		perror("Cannot get socket");
		goto err;
	}

	struct ethtool_drvinfo drvinfo;
	memset(&drvinfo, 0, sizeof(drvinfo));
	drvinfo.cmd = ETHTOOL_GDRVINFO;
	ifr.ifr_data = (caddr_t)&drvinfo;
	if(ioctl(fd, SIOCETHTOOL, &ifr) < 0) {
		perror("Cannot get driver info");
		goto err;
	}

	unsigned int n_stats, sz_str, sz_stats;
	n_stats = drvinfo.n_stats;
	if(n_stats < 1) {
		fprintf(stderr, "no stats available\n");
		goto err;
	}
	
	sz_str = n_stats * ETH_GSTRING_LEN;
	struct ethtool_gstrings* strings;
	strings = calloc(1, sz_str + sizeof(struct ethtool_gstrings));

	if(!strings) {
		fprintf(stderr, "no memory available\n");
		goto err;
	}

	strings->cmd = ETHTOOL_GSTRINGS;
	strings->string_set = ETH_SS_STATS;
	strings->len = n_stats;
	ifr.ifr_data = (caddr_t) strings;
	if (ioctl(fd, SIOCETHTOOL, &ifr) < 0) {
		perror("Cannot get stats strings info");
		goto err;
	}

	/*
	sz_stats = n_stats * sizeof(__u64);	
	struct ethtool_stats* stats;
	stats = calloc(1, sz_stats + sizeof(struct ethtool_stats));
	if(!stats) {
		fprintf(stderr, "no memory available\n");
		goto err;
	}

	stats->cmd = ETHTOOL_GSTATS;
	stats->n_stats = n_stats;
	ifr.ifr_data = (caddr_t) stats;
	*/
	prepare_request(n_stats, &ifr);
	read_counters(fd, &ifr);

	//dump_stats(n_stats, strings, stats);
	__u64 tx_packs[PRIO_NUM];
	__u64 tx_bytes[PRIO_NUM];
	__u64 rx_packs[PRIO_NUM];
	__u64 rx_bytes[PRIO_NUM];

	load_stats(strings, stats, tx_packs, tx_bytes, rx_packs, rx_bytes, PRIO_NUM);
	free(stats);

	close(fd);
	free(strings);
	return 0;

err:
	close(fd);
	free(stats);
	free(strings);
	return -1;
}
