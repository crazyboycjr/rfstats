#ifndef COUNTER_H_
#define COUNTER_H_

#include "ethtool.h"
#include "prism/logging.h"
#include "config.h"

#include <chrono>

using namespace std::chrono;
using Clock = high_resolution_clock;

struct Counter {
  Counter(struct ethtool_gstrings* strings, const char* key_)
      : bytes{0},
        delta{0},
        last_ts{Clock::now().time_since_epoch().count()},
        index{-1},
        cnt{0} {
    strncpy(key, key_, ETH_GSTRING_LEN);

    base_index = FindIndex(strings, RX_PRIO0_BYTES);
    index = FindIndex(strings, key) - base_index;
    // printf("index = %d\n", index);
    CHECK(index >= 0);
  }

  void Update(struct ethtool_stats* stats) {
    delta = stats->data[index + base_index] - bytes;
    bytes = stats->data[index + base_index];
  }

  inline int GetCqeIndex(int index) { return index % 4 * PRIO_NUM + index / 4; }

  void Update(CQE* cqe) {
    // to make it print less frequently
    if ((++cnt & 0xff) == 0xff) {
      auto val = *(cqe->rx_bytes + GetCqeIndex(index));
      delta = val - bytes;
      bytes = val;
      if (delta == bytes && bytes != 0) return;

      // auto now = Clock::now();
      // long dura_ns = (now - tp).count();
      // //print_rates(dura_ns, strings, stats);
      // printf("tx: %ld %.3fMB/s\n", now.time_since_epoch().count(), 1e3 * delta / dura_ns);
      long dura_ns = cqe->ts - last_ts;
      printf("%s: ts=%ld, bytes=%ld, delta=%ld, speed=%.3fMB/s\n", key, cqe->ts,
             bytes, delta, 1e3 * delta / dura_ns);
      last_ts = cqe->ts;
    }
  }

  int FindIndex(struct ethtool_gstrings* strings, const char* key_) {
    int n_stats = strings->len;
    int i;
    for (i = 0; i < n_stats; i++) {
      if (!strcmp(key_, reinterpret_cast<char*>(strings->data +
                                                i * ETH_GSTRING_LEN))) {
        break;
      }
    }
    CHECK_LT(i, n_stats);
    return i;
  }

  size_t bytes;
  size_t delta;
  //Clock::time_point last_tp;
  long last_ts;
  int index;
  int cnt;
  char key[ETH_GSTRING_LEN];

 private:
  int base_index;
};


#endif  // COUNTER_H_
