
#ifndef WORKER_POOL_H_
#define WORKER_POOL_H_

#include "worker_thread.h"
#include "app_context.h"

#include <memory>
#include <vector>


class WorkerPool {
 public:
  WorkerPool(AppContext& app_ctx, int num_workers)
      : app_ctx_{app_ctx},
        num_workers_{num_workers},
        thread_pool_{static_cast<size_t>(num_workers)} {}

  ~WorkerPool() {}

  void CreateWorkerThreads() {
    for (int i = 0; i < num_workers_; i++) {
      thread_pool_[i] = std::make_unique<WorkerThread>(app_ctx_, RX_PRIO0_BYTES);
    }
  }

  void StartWorkerThreads() {
    for (int i = 0; i < num_workers_; i++) {
      thread_pool_[i]->Start();
    }
  }

  void StopWorkerThreads() {
    for (int i = 0; i < num_workers_; i++) {
      thread_pool_[i]->Terminate();
    }
  }

  inline int num_workers() const { return num_workers_; }
  inline WorkerThread* worker_thread(int i) { return thread_pool_[i].get(); }

 private:
  AppContext& app_ctx_;
  int num_workers_;
  std::vector<std::unique_ptr<WorkerThread>> thread_pool_;
};

#endif  // WORKER_POOL_H_