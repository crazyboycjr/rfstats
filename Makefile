
.PHONY: clean

INCPATH = -I.
CFLAGS := -g -std=c++17 -Wall -Werror
LDFLAGS := -lpthread

SRCS = rfstats.cc config.cc worker_thread.cc
OBJS = $(patsubst %,build/%,$(SRCS:.cc=.o))

APP := rfstats

all: $(APP)

rfstats: $(OBJS)
	$(CXX) $(INCPATH) $(CFLAGS) $^ -o $@ $(LDFLAGS)

build/%.o: %.cc
	@mkdir -p $(@D)
	$(CXX) $(INCPATH) -std=c++17 -MM -MT build/$*.o $< >build/$*.d
	$(CXX) $(INCPATH) $(CFLAGS) -c $< -o $@

-include build/*.d
-include build/*/*.d


#rfstats: rfstats.cc config.cc
#	$(CXX) $^ -o $@ $(CFLAGS) $(LDFLAGS)

clean:
	rm -rfv $(OBJS)
	rm -rfv $(OBJS:.o=.d)
	rm -rfv $(APP)
