#ifndef APP_CONTEXT_H_
#define APP_CONTEXT_H_

#include "ethtool.h"
#include "config.h"
#include <memory>

class WorkerPool;
class CollectorThread;

class AppContext {
 public:
  AppContext();

  ~AppContext();

  inline Config& conf() { return conf_; }
  inline struct cmd_context& cmd_ctx() { return cmd_ctx_; }
  inline struct ethtool_gstrings* counter_names() { return counter_names_; }
  inline WorkerPool* worker_pool() { return worker_pool_.get(); }

  int Run();

  int Exit();

 private:
  Config conf_;
  struct cmd_context cmd_ctx_;
  struct ethtool_gstrings *counter_names_;
  std::unique_ptr<CollectorThread> collector_th_;
  std::unique_ptr<WorkerPool> worker_pool_;
};

#endif  // APP_CONTEXT_H_
