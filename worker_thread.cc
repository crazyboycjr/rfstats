#include "worker_thread.h"

WorkerThread::WorkerThread(AppContext& app_ctx, const char* key)
    : app_ctx_{app_ctx}, wq_{}, cq_{} {
  FindIndex(app_ctx.counter_names(), key);
  PrepareRequest(app_ctx.counter_names()->len);
}

WorkerThread::~WorkerThread() {
  if (stats_) {
    free(stats_);
    stats_ = nullptr;
  }
}

void WorkerThread::Run() {
  WQE wqe;
  CQE cqe;
  while (!terminated_.load()) {
    if (!wq_.TryPop(&wqe)) continue;
    (void)wqe;
    // read counters
    cqe.ts = Clock::now().time_since_epoch().count();
    PCHECK(!SendIfReq());
    LoadStats(&cqe, PRIO_NUM);
    cq_.Push(cqe);
  }
}

void WorkerThread::LoadStats(CQE* cqe, size_t size) {
  size_t j = index_;
  for (size_t i = 0; i < size; ++i) {
    cqe->rx_bytes[i] = stats_->data[j++];
    cqe->rx_packs[i] = stats_->data[j++];
    cqe->tx_bytes[i] = stats_->data[j++];
    cqe->tx_packs[i] = stats_->data[j++];
  }
}

void WorkerThread::PrepareRequest(unsigned int n_stats) {
  unsigned int sz_stats = n_stats * sizeof(__u64);

  stats_ = static_cast<struct ethtool_stats*>(
      calloc(1, sz_stats + sizeof(struct ethtool_stats)));
  if (!stats_) {
    fprintf(stderr, "no memory available\n");
    exit(1);
  }

  stats_->cmd = ETHTOOL_GSTATS;
  stats_->n_stats = n_stats;

  memcpy(&ctx_, &app_ctx_.cmd_ctx(), sizeof(struct cmd_context));

  ctx_.ifr.ifr_data = reinterpret_cast<char*>(stats_);
}

void WorkerThread::FindIndex(struct ethtool_gstrings* strings, const char* key) {
  int n_stats = strings->len;
  int i;
  for (i = 0; i < n_stats; i++) {
    if (!strcmp(key, reinterpret_cast<char*>(strings->data +
                                              i * ETH_GSTRING_LEN))) {
      break;
    }
  }

  index_ = i;
  CHECK_LT(index_, n_stats) << key << " not found";
}